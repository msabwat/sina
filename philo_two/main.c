/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 14:23:09 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/05 20:12:14 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

#include "philo_two.h"

static void		cleanup_simulation(t_sys *sys)
{
	if (sys->philos)
		free(sys->philos);
	free(sys);
}

static void		semaphore_init(sem_t **sem, int i, const char *name, int value)
{
	char *tmp;
	char *result;

	tmp = ft_itoa(i);
	result = ft_strjoin(name, tmp);
	*sem = sem_open(result, O_CREAT | O_EXCL, 0644, value);
	sem_unlink(result);
	free(result);
	free(tmp);
}

static void		semaphores_init(t_sys *sys)
{
	int i;

	i = 0;
	sys->forks = sem_open("fork", O_CREAT | O_EXCL, 0644, sys->size);
	sem_unlink("fork");
	sys->m_forks = sem_open("mutex_forks", O_CREAT | O_EXCL, 0644, 1);
	sem_unlink("mutex_forks");
	while (i < sys->size)
	{
		sys->philos[i].forks = sys->forks;
		sys->philos[i].m_forks = sys->m_forks;
		semaphore_init(&(sys->philos[i].m_time), i, "m_time", 1);
		semaphore_init(&(sys->philos[i].m_state), i, "m_state", 1);
		i++;
	}
}

static int		init_sys(t_sys *sys, int ac, char **av)
{
	sys->size = ft_atoi(av[1]);
	sys->max_eat = (ac == 6) ? ft_atoi(av[5]) : -1;
	sys->philos = 0;
	sys->time.start = 0;
	sys->time.t_todie = ft_atoi(av[2]);
	sys->time.t_toeat = ft_atoi(av[3]);
	sys->time.t_tosleep = ft_atoi(av[4]);
	sys->philos = (t_phi*)malloc(sizeof(t_phi) * sys->size);
	if (!(sys->philos))
	{
		free(sys);
		return (0);
	}
	return (1);
}

int				main(int ac, char **av)
{
	t_sys		*sys;

	if ((ac < 5) || (ac > 6))
	{
		print_usage();
		return (1);
	}
	sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		return (1);
	if (!check_options(ac, av))
	{
		print_usage();
		free(sys);
		return (1);
	}
	if (!init_sys(sys, ac, av))
		return (1);
	semaphores_init(sys);
	setup_table(sys);
	tour_de_table(sys);
	cleanup_simulation(sys);
	return (0);
}
