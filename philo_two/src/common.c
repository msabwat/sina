/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:11:07 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/07 16:12:30 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/time.h>
#include <stdio.h>
#include "philo_two.h"

long		get_time(void)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return (time.tv_sec * 1000 + time.tv_usec / 1000);
}

void		ft_usleep(int duration)
{
	long start;
	long current;

	start = get_time();
	current = start;
	while (duration > current - start)
	{
		usleep(100);
		current = get_time();
	}
}

static int	check_option_isnum(char *av)
{
	int j;

	j = 0;
	while (j < (int)ft_strlen(av))
	{
		if (!ft_isdigit(av[j++]))
		{
			printf("wrong argument: expected numbers\n");
			return (0);
		}
	}
	if (ft_atoi(av) < 0)
	{
		printf("wrong argument: expected positive number %s\n", av);
		return (0);
	}
	return (1);
}

int			check_options(int ac, char **av)
{
	int i;

	i = 1;
	while (i < ac)
	{
		if (!check_option_isnum(av[i++]))
			return (0);
	}
	if (ft_atoi(av[1]) > MAX_THREADS)
	{
		printf("table cannot feed more than %d philosophers\n", MAX_THREADS);
		return (0);
	}
	else if (ft_atoi(av[1]) < 2)
	{
		printf("table cannot feed less than 2 philosophers\n");
		return (0);
	}
	else if ((ac == 6) && (ft_atoi(av[5]) == 0))
	{
		printf("wrong meal number, cannot be 0\n");
		return (0);
	}
	return (1);
}

int			check_state(t_phi *philo)
{
	t_state state;

	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
		return (0);
	return (1);
}
