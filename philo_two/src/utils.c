/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:56:29 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/05 14:16:13 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "philo_two.h"

int			ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

size_t		ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		s++;
		i++;
	}
	return (i);
}

static int	toskip(char c)
{
	if ((c <= 32 && c != 27 && c != '\200') && c != '\0')
		return (1);
	return (0);
}

int			ft_atoi(const char *str)
{
	int	cnt;
	int	number;
	int	s;

	s = 0;
	number = 0;
	cnt = 0;
	while (toskip(str[cnt]))
		cnt++;
	if (str[cnt] == '+' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9')
		cnt++;
	if (str[cnt] == '-' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9' && s == 0)
	{
		cnt++;
		s = 1;
	}
	while ((str[cnt] >= '0' && str[cnt] <= '9') && str[cnt] != '\0')
	{
		if (s == 0)
			number = 10 * number + str[cnt] - 48;
		else
			number = 10 * number - str[cnt] + 48;
		cnt++;
	}
	return (number);
}

void		print_usage(void)
{
	printf("usage: ./philo_X number_of_philosophers");
	printf(" time_to_die time_to_eat time_to_sleep ");
	printf("[number_of_time_each_philosopher_must_eat]\n");
}
