/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 14:35:45 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/08 13:39:42 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "philo_two.h"

static int		p_take_forks(t_phi *philo)
{
	if (!check_state(philo))
		return (0);
	sem_wait(philo->m_forks);
	if (!check_state(philo))
		return (0);
	sem_wait(philo->forks);
	if (!check_state(philo))
		return (0);
	printf("%ld %d has taken a fork\n", get_time() - philo->p_time.start,
					philo->id + 1);
	if (!check_state(philo))
		return (0);
	sem_wait(philo->forks);
	if (!check_state(philo))
		return (0);
	printf("%ld %d has taken a fork\n", get_time() - philo->p_time.start,
					philo->id + 1);
	sem_post(philo->m_forks);
	return (1);
}

static int		p_drop_forks(t_phi *philo)
{
	t_state state;

	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
	{
		sem_post(philo->forks);
		sem_post(philo->forks);
		return (0);
	}
	sem_post(philo->forks);
	sem_post(philo->forks);
	return (1);
}

static int		p_eat(t_phi *philo)
{
	t_state state;

	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	sem_wait(philo->m_time);
	philo->ts = get_time();
	sem_post(philo->m_time);
	if (state == DEAD)
		return (0);
	printf("%ld %d is eating\n", get_time() - philo->p_time.start,
					philo->id + 1);
	ft_usleep(philo->p_time.t_toeat);
	philo->cnt_eat += 1;
	return (1);
}

static int		p_sleep(t_phi *philo)
{
	t_state state;

	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
		return (0);
	printf("%ld %d is sleeping\n", get_time() - philo->p_time.start,
					philo->id + 1);
	ft_usleep(philo->p_time.t_tosleep);
	return (1);
}

void			*run_philo(void *arg)
{
	t_phi	*philo;

	philo = (t_phi *)arg;
	while (42)
	{
		if (!p_take_forks(philo))
			break ;
		if (!p_eat(philo))
		{
			p_drop_forks(philo);
			break ;
		}
		p_drop_forks(philo);
		if (philo->cnt_eat == philo->max_eat)
		{
			sem_wait(philo->m_state);
			philo->state = FINISHED;
			sem_post(philo->m_state);
			break ;
		}
		if (!p_sleep(philo))
			break ;
	}
	return (0);
}
