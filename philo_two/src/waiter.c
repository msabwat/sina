/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   waiter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 14:51:59 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/08 13:08:31 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include "philo_two.h"

static void		croquemort(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		sem_wait(sys->philos[i].m_state);
		sys->philos[i].state = DEAD;
		sem_post(sys->philos[i].m_state);
		i++;
	}
	i = 0;
	while (i < sys->size)
	{
		sem_post(sys->philos[i].forks);
		sem_post(sys->philos[i].forks);
		sem_post(sys->philos[i].forks);
		sem_post(sys->philos[i].forks);
		sem_post(sys->philos[i].forks);
		sem_post(sys->philos[i].m_time);
		sem_post(sys->philos[i].m_state);
		sem_post(sys->philos[i].m_forks);
		pthread_join(sys->philos[i].t_id, NULL);
		i++;
	}
}

static int		check_time(t_sys *sys)
{
	int		i;
	long	time;

	i = 0;
	while (i < sys->size)
	{
		sem_wait(sys->philos[i].m_time);
		time = get_time() - sys->philos[i].ts;
		sem_post(sys->philos[i].m_time);
		if (time > sys->time.t_todie)
		{
			printf("%ld %d died\n", get_time() - sys->time.start, i + 1);
			sem_wait(sys->philos[i].m_state);
			sys->philos[i].state = DEAD;
			sem_post(sys->philos[i].m_state);
			return (0);
		}
		i++;
	}
	return (1);
}

static int		check_mealcount(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		sem_wait(sys->philos[i].m_state);
		if (sys->philos[i].state != FINISHED)
		{
			break ;
		}
		if ((sys->philos[i].state == FINISHED) && (i == sys->size - 1))
		{
			sem_post(sys->philos[i].m_state);
			return (0);
		}
		sem_post(sys->philos[i].m_state);
		i++;
	}
	sem_post(sys->philos[i].m_state);
	return (1);
}

void			tour_de_table(t_sys *sys)
{
	while (42)
	{
		if (!check_time(sys))
			break ;
		if (!check_mealcount(sys))
			break ;
	}
	croquemort(sys);
}

void			setup_table(t_sys *sys)
{
	int i;

	i = 0;
	sys->time.start = get_time();
	while (i < sys->size)
	{
		sys->philos[i].id = i;
		sys->philos[i].ts = sys->time.start;
		sys->philos[i].state = INIT;
		sys->philos[i].cnt_eat = 0;
		sys->philos[i].max_eat = sys->max_eat;
		sys->philos[i].p_time = sys->time;
		pthread_create(&(sys->philos[i].t_id),
						NULL, &run_philo, &(sys->philos[i]));
		i++;
	}
}
