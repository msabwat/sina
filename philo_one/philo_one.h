/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_one.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 14:59:03 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/06 21:12:14 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_ONE_H
# define PHILO_ONE_H

# include <pthread.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/time.h>

# define MAX_THREADS 200

typedef enum		e_state
{
	INIT,
	THINKING,
	EATING,
	SLEEPING,
	DEAD,
	FINISHED,
}					t_state;

typedef struct		s_time
{
	long			start;
	long			t_todie;
	long			t_toeat;
	long			t_tosleep;
}					t_time;

typedef struct		s_phi
{
	int				id;
	pthread_t		t_id;
	long			ts;
	t_state			state;
	int				max_eat;
	t_time			p_time;
	int				eat_cnt;
	pthread_mutex_t	lock;
	pthread_mutex_t	l_fork;
	pthread_mutex_t	*r_fork;
}					t_phi;

typedef struct		s_sys
{
	int				size;
	int				max_eat;
	t_time			time;
	t_phi			*philos;
}					t_sys;

int					ft_atoi(const char *str);
int					ft_isdigit(int c);
size_t				ft_strlen(const char *s);
long				get_time(void);
void				ft_usleep(int duration);
int					check_options(int ac, char **av);
int					check_state(t_phi *philo);
void				print_usage();

void				tour_de_table(t_sys *sys);
int					p_take_forks(t_phi *philo);
void				setup_table(t_sys *sys);
void				*run_philo(void *arg);
#endif
