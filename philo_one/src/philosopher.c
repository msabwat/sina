/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 15:10:59 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/07 02:49:33 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "philo_one.h"

static int		p_drop_forks(t_phi *philo)
{
	t_state state;

	pthread_mutex_lock(&(philo->lock));
	state = philo->state;
	pthread_mutex_unlock(&(philo->lock));
	if (state == DEAD)
	{
		pthread_mutex_unlock(&(philo->l_fork));
		pthread_mutex_unlock(philo->r_fork);
		return (0);
	}
	pthread_mutex_unlock(philo->r_fork);
	pthread_mutex_unlock(&(philo->l_fork));
	return (1);
}

static int		p_eat(t_phi *philo)
{
	int		id;
	t_state state;

	id = philo->id;
	pthread_mutex_lock(&(philo->lock));
	state = philo->state;
	philo->ts = get_time();
	pthread_mutex_unlock(&(philo->lock));
	if (state == DEAD)
		return (0);
	printf("%ld %d is eating\n", get_time() - philo->p_time.start, id + 1);
	ft_usleep(philo->p_time.t_toeat);
	philo->eat_cnt += 1;
	return (1);
}

static int		p_sleep(t_phi *philo)
{
	int		id;
	t_state state;

	id = philo->id;
	pthread_mutex_lock(&(philo->lock));
	state = philo->state;
	pthread_mutex_unlock(&(philo->lock));
	if (state == DEAD)
		return (0);
	printf("%ld %d is sleeping\n", get_time() - philo->p_time.start, id + 1);
	ft_usleep(philo->p_time.t_tosleep);
	return (1);
}

void			*run_philo(void *arg)
{
	t_phi	*philo;

	philo = (t_phi *)arg;
	while (42)
	{
		if (!p_take_forks(philo))
			break ;
		if (!p_eat(philo))
		{
			p_drop_forks(philo);
			break ;
		}
		p_drop_forks(philo);
		if (philo->eat_cnt == philo->max_eat)
		{
			pthread_mutex_lock(&(philo->lock));
			philo->state = FINISHED;
			pthread_mutex_unlock(&(philo->lock));
			break ;
		}
		if (!p_sleep(philo))
			break ;
	}
	return (0);
}
