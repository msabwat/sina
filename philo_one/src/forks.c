/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   forks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/06 20:49:30 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/07 02:50:11 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "philo_one.h"

#define FORK_MSG " has taken a fork"

static int				righty_lock(t_phi *philo, int id)
{
	pthread_mutex_lock(philo->r_fork);
	if (!check_state(philo))
	{
		pthread_mutex_unlock(philo->r_fork);
		return (0);
	}
	printf("%ld %d"FORK_MSG"\n", get_time() - philo->p_time.start, id + 1);
	pthread_mutex_lock(&(philo->l_fork));
	if (!check_state(philo))
	{
		pthread_mutex_unlock(philo->r_fork);
		pthread_mutex_unlock(&(philo->l_fork));
		return (0);
	}
	printf("%ld %d"FORK_MSG"\n", get_time() - philo->p_time.start, id + 1);
	return (1);
}

static int				lefty_lock(t_phi *philo, int id)
{
	pthread_mutex_lock(&(philo->l_fork));
	if (!check_state(philo))
	{
		pthread_mutex_unlock(&(philo->l_fork));
		return (0);
	}
	printf("%ld %d"FORK_MSG"\n", get_time() - philo->p_time.start, id + 1);
	pthread_mutex_lock(philo->r_fork);
	if (!check_state(philo))
	{
		pthread_mutex_unlock(philo->r_fork);
		pthread_mutex_unlock(&(philo->l_fork));
		return (0);
	}
	printf("%ld %d"FORK_MSG"\n", get_time() - philo->p_time.start, id + 1);
	return (1);
}

int						p_take_forks(t_phi *philo)
{
	int		id;

	id = philo->id;
	if (!check_state(philo))
		return (0);
	if ((philo->id & 1) == 0)
	{
		if (!righty_lock(philo, id))
			return (0);
	}
	else
	{
		if (!lefty_lock(philo, id))
			return (0);
	}
	return (1);
}
