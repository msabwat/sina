/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   waiter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 15:08:27 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/05 15:29:00 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "philo_one.h"

static void		croquemort(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		pthread_mutex_lock(&(sys->philos[i].lock));
		sys->philos[i].state = DEAD;
		pthread_mutex_unlock(&(sys->philos[i].lock));
		i++;
	}
	i = 0;
	while (i < sys->size)
	{
		pthread_join(sys->philos[i].t_id, NULL);
		i++;
	}
}

static int		check_time(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		pthread_mutex_lock(&(sys->philos[i].lock));
		if (get_time() - sys->philos[i].ts > sys->time.t_todie)
		{
			printf("%ld %d died\n", get_time() - sys->time.start, i);
			sys->philos[i].state = DEAD;
			pthread_mutex_unlock(&(sys->philos[i].lock));
			return (0);
		}
		pthread_mutex_unlock(&(sys->philos[i].lock));
		i++;
	}
	return (1);
}

static int		check_mealcount(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		pthread_mutex_lock(&(sys->philos[i].lock));
		if (sys->philos[i].state != FINISHED)
		{
			pthread_mutex_unlock(&(sys->philos[i].lock));
			break ;
		}
		if ((sys->philos[i].state == FINISHED) && (i == sys->size - 1))
		{
			pthread_mutex_unlock(&(sys->philos[i].lock));
			return (0);
		}
		pthread_mutex_unlock(&(sys->philos[i].lock));
		i++;
	}
	return (1);
}

void			tour_de_table(t_sys *sys)
{
	while (42)
	{
		if (!check_time(sys))
			break ;
		if (!check_mealcount(sys))
			break ;
	}
	croquemort(sys);
}

void			setup_table(t_sys *sys)
{
	int i;

	i = 0;
	sys->time.start = get_time();
	while (i < sys->size)
	{
		sys->philos[i].id = i;
		sys->philos[i].ts = sys->time.start;
		sys->philos[i].state = INIT;
		sys->philos[i].max_eat = sys->max_eat;
		sys->philos[i].eat_cnt = 0;
		sys->philos[i].p_time = sys->time;
		pthread_create(&(sys->philos[i].t_id),
							NULL, &run_philo, &(sys->philos[i]));
		i++;
	}
}
