/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 15:00:26 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/05 15:05:12 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "philo_one.h"

static void		free_sys(t_sys *sys)
{
	if (sys->philos)
		free(sys->philos);
	free(sys);
}

static void		cleanup_simulation(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		pthread_mutex_destroy(&(sys->philos[i].lock));
		pthread_mutex_destroy(&(sys->philos[i].l_fork));
		i++;
	}
	free_sys(sys);
}

static void		mutexes_init(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		pthread_mutex_init(&(sys->philos[i].lock), NULL);
		pthread_mutex_init(&(sys->philos[i].l_fork), NULL);
		i++;
	}
	i = 0;
	while (i < sys->size)
	{
		if (i == sys->size - 1)
			sys->philos[i].r_fork = &(sys->philos[0].l_fork);
		else
			sys->philos[i].r_fork = &(sys->philos[i + 1].l_fork);
		i++;
	}
}

static int		init_sys(t_sys *sys, int ac, char **av)
{
	sys->size = ft_atoi(av[1]);
	sys->max_eat = (ac == 6) ? ft_atoi(av[5]) : -1;
	sys->philos = 0;
	sys->time.start = 0;
	sys->time.t_todie = ft_atoi(av[2]);
	sys->time.t_toeat = ft_atoi(av[3]);
	sys->time.t_tosleep = ft_atoi(av[4]);
	sys->philos = (t_phi*)malloc(sizeof(t_phi) * sys->size);
	if (!(sys->philos))
	{
		free_sys(sys);
		return (0);
	}
	return (1);
}

int				main(int ac, char **av)
{
	t_sys		*sys;

	if ((ac < 5) || (ac > 6))
	{
		print_usage();
		return (1);
	}
	sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		return (1);
	if (!check_options(ac, av))
	{
		print_usage();
		free(sys);
		return (1);
	}
	if (!init_sys(sys, ac, av))
		return (1);
	mutexes_init(sys);
	setup_table(sys);
	tour_de_table(sys);
	cleanup_simulation(sys);
	return (0);
}
