/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 12:57:29 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/08 13:18:46 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "philo_three.h"

static void		cleanup_simulation(t_sys *sys)
{
	if (sys->philos)
		free(sys->philos);
	free(sys);
}

static void		semaphore_init(sem_t **sem, int i, const char *name, int value)
{
	char *tmp;
	char *result;

	tmp = ft_itoa(i);
	result = ft_strjoin(name, tmp);
	*sem = sem_open(result, O_CREAT | O_EXCL, 0644, value);
	sem_unlink(result);
	free(result);
	free(tmp);
}

static void		semaphores_init(t_sys *sys)
{
	int i;

	i = 0;
	sys->forks = sem_open("frk", O_CREAT | O_EXCL, 0644, sys->size);
	sem_unlink("frk");
	sys->terminator = sem_open("terminator", O_CREAT | O_EXCL, 0644, 0);
	sem_unlink("terminator");
	sys->m_forks = sem_open("mutex_forks", O_CREAT | O_EXCL, 0644, 1);
	sem_unlink("mutex_forks");
	while (i < sys->size)
	{
		semaphore_init(&(sys->philos[i].philo_ready), i, "philo_ready", 0);
		semaphore_init(&(sys->philos[i].monitor_ready), i, "monitor_ready", 0);
		semaphore_init(&(sys->philos[i].m_time), i, "m_time", 1);
		semaphore_init(&(sys->philos[i].m_state), i, "m_state", 1);
		i++;
	}
}

static int		init_sys(t_sys *sys, int ac, char **av)
{
	sys->size = ft_atoi(av[1]);
	sys->max_eat = (ac == 6) ? ft_atoi(av[5]) : -1;
	sys->philos = 0;
	sys->time.start = 0;
	sys->time.t_todie = ft_atoi(av[2]);
	sys->time.t_toeat = ft_atoi(av[3]);
	sys->time.t_tosleep = ft_atoi(av[4]);
	sys->philos = (t_phi*)malloc(sizeof(t_phi) * sys->size);
	if (!(sys->philos))
	{
		free(sys);
		return (0);
	}
	return (1);
}

int				main(int ac, char **av)
{
	t_sys		*sys;

	if ((ac < 5) || (ac > 6))
	{
		print_usage();
		return (1);
	}
	sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		return (1);
	if (!check_options(ac, av))
	{
		print_usage();
		free(sys);
		return (1);
	}
	if (!init_sys(sys, ac, av))
		return (1);
	semaphores_init(sys);
	setup_table(sys);
	cleanup_simulation(sys);
	return (0);
}
