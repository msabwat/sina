/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   waiter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:32:06 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/05 17:39:46 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include "philo_three.h"

static void		*monitor(void *arg)
{
	t_phi	*philo;
	long	time;
	t_sys	*sys;

	philo = (t_phi *)arg;
	sys = philo->sys;
	sem_wait(philo->monitor_ready);
	while (42)
	{
		sem_wait(philo->m_time);
		time = get_time() - philo->ts;
		sem_post(philo->m_time);
		if (time > sys->time.t_todie)
		{
			printf("%ld %d died\n", get_time() - sys->time.start,
									philo->id + 1);
			sem_wait(philo->m_state);
			philo->state = DEAD;
			sem_post(philo->m_state);
			break ;
		}
	}
	sem_post(sys->terminator);
	return (NULL);
}

static void		*terminator(void *arg)
{
	t_sys	*sys;
	int		i;

	i = 0;
	sys = (t_sys *)arg;
	sem_wait(sys->terminator);
	while (i < sys->size)
	{
		sem_wait(sys->philos[i].m_state);
		sys->philos[i].state = DEAD;
		sem_post(sys->philos[i].m_state);
		kill(sys->philos[i].pid, SIGINT);
		i++;
	}
	return (NULL);
}

static void		create_philosopher(t_sys *sys, int i)
{
	int pid;

	pid = fork();
	if (pid == 0)
	{
		pthread_create(&(sys->philos[i].tid), NULL,
										&monitor, &(sys->philos[i]));
		run_philo(&(sys->philos[i]));
		exit(0);
	}
	else if (pid > 0)
		sys->philos[i].pid = pid;
	else
	{
		printf("could not fork\n");
		exit(1);
	}
}

static void		init_philo(t_sys *sys, int i)
{
	sys->philos[i].id = i;
	sys->philos[i].state = INIT;
	sys->philos[i].cnt_eat = 0;
	sys->philos[i].ts = sys->time.start;
	sys->philos[i].sys = sys;
}

void			setup_table(t_sys *sys)
{
	int			i;
	pthread_t	id;

	i = 0;
	sys->time.start = get_time();
	while (i < sys->size)
		init_philo(sys, i++);
	i = 0;
	while (i < sys->size)
		create_philosopher(sys, i++);
	i = 0;
	while (i < sys->size)
		sem_post(sys->philos[i++].philo_ready);
	pthread_create(&id, NULL, &terminator, sys);
	pthread_detach(id);
	i = 0;
	while (i < sys->size)
		waitpid(sys->philos[i++].pid, NULL, 0);
}
