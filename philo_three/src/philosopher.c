/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:48:28 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/08 13:19:41 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include "philo_three.h"

static int		p_take_forks(t_phi *philo)
{
	t_sys	*sys;
	t_state	state;

	sys = philo->sys;
	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
		return (0);
	sem_wait(sys->m_forks);
	sem_wait(sys->forks);
	printf("%ld %d has taken a fork\n",
			get_time() - sys->time.start, philo->id + 1);
	sem_wait(sys->forks);
	printf("%ld %d has taken a fork\n",
			get_time() - sys->time.start, philo->id + 1);
	sem_post(sys->m_forks);
	return (1);
}

static int		p_drop_forks(t_phi *philo)
{
	t_sys	*sys;
	t_state	state;

	sys = philo->sys;
	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
	{
		sem_post(sys->forks);
		sem_post(sys->forks);
		return (0);
	}
	sem_post(sys->forks);
	sem_post(sys->forks);
	return (1);
}

static int		p_eat(t_phi *philo)
{
	t_sys	*sys;
	t_state	state;

	sys = philo->sys;
	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
		return (0);
	sem_wait(philo->m_time);
	philo->ts = get_time();
	sem_post(philo->m_time);
	printf("%ld %d is eating\n", get_time() - sys->time.start, philo->id + 1);
	ft_usleep(sys->time.t_toeat);
	philo->cnt_eat += 1;
	return (1);
}

static int		p_sleep(t_phi *philo)
{
	t_sys	*sys;
	t_state	state;

	sys = philo->sys;
	sem_wait(philo->m_state);
	state = philo->state;
	sem_post(philo->m_state);
	if (state == DEAD)
		return (0);
	printf("%ld %d is sleeping\n", get_time() - sys->time.start, philo->id + 1);
	ft_usleep(sys->time.t_tosleep);
	return (1);
}

void			*run_philo(t_phi *philo)
{
	t_sys *sys;

	sys = philo->sys;
	sem_wait(philo->philo_ready);
	sem_post(philo->monitor_ready);
	while (42)
	{
		if (!p_take_forks(philo))
			break ;
		if (!p_eat(philo))
		{
			p_drop_forks(philo);
			break ;
		}
		p_drop_forks(philo);
		if (philo->cnt_eat == sys->max_eat)
			break ;
		if (!p_sleep(philo))
			break ;
	}
	return (0);
}
