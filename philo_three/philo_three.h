/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_three.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:33:41 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/08 13:19:59 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_THREE_H
# define PHILO_THREE_H

# include <pthread.h>
# include <semaphore.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/time.h>

# define MAX_THREADS 200

typedef enum		e_state
{
	INIT,
	THINKING,
	EATING,
	SLEEPING,
	DEAD,
	FINISHED,
}					t_state;

typedef struct		s_time
{
	long			start;
	long			t_todie;
	long			t_toeat;
	long			t_tosleep;
}					t_time;

typedef struct		s_phi
{
	int				id;
	pthread_t		tid;
	int				pid;
	long			ts;
	t_state			state;
	int				cnt_eat;
	t_time			p_time;
	sem_t			*philo_ready;
	sem_t			*monitor_ready;
	sem_t			*m_time;
	sem_t			*m_state;
	void			*sys;
}					t_phi;

typedef struct		s_sys
{
	int				size;
	int				max_eat;
	t_time			time;
	t_phi			*philos;
	sem_t			*forks;
	sem_t			*terminator;
	sem_t			*m_forks;
}					t_sys;

char				*ft_strdup(const char *s1);
int					ft_atoi(const char *str);
char				*ft_itoa(int n);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strcpy(char *dst, const char *src);
int					ft_isdigit(int c);
size_t				ft_strlen(const char *s);
long				get_time(void);
void				ft_usleep(int duration);
int					check_options(int ac, char **av);
void				print_usage();

void				tour_de_table(t_sys *sys);
void				setup_table(t_sys *sys);
void				*run_philo(t_phi *philo);
#endif
